#include "ecs.h"
#include "ecsinternal.h"

static inline bool SystemLoop(EcsHeader * ecs, System * system);

ID EcsSystemCreate(EcsHeader * ecs, void *(*onEventFunc)(EventType, EntityID, Vectorv *), const char * componentsFormat, int componentCount, ...)
{
    if(ecs)
    {
        System system;
    system.onEventFunc = onEventFunc; 
    system.isActive = true;
    system.isDisposed = false;

        VectorvCreate(&system.trackedComponentsIDs, sizeof(ID), NULL);
      
        // Parse va_list, assume componentIDs are provided and push them into the vector
        va_list trackedComponentsIDs;
        va_start(trackedComponentsIDs, (int) componentCount);

        assert(componentCount == strlen(componentsFormat));

        for(ID i = 0; i < componentCount; ++i)
        {
            ID componentID = va_arg(trackedComponentsIDs, unsigned);
            VectorvPush(&system.trackedComponentsIDs, &componentID);
        }

        va_end(trackedComponentsIDs);

        VectorvPush(&ecs->systems, &system);
        return ecs->systems.length - 1;
    }
    return ID_INVALID;
}

bool EcsSystemDestroy(EcsHeader * ecs, ID systemID)
{
    if(ecs && systemID != ID_INVALID)
    {
        System * system = VectorvAt(&ecs->systems, systemID);
        if(system && !system->isDisposed)
        {
            VectorvDestroy(&system->trackedComponentsIDs);
            system->isActive = false;
            system->isDisposed = true;
            return true;
        }
    }
    return false;
}

bool EcsSystemActiveSet(EcsHeader * ecs, ID systemID, bool isActive)
{
    if(ecs && systemID != ID_INVALID)
    {
        System * system = VectorvAt(&ecs->systems, systemID);
        if(system && !system->isDisposed)
        {
            system->isActive = isActive;
            return true;
        }
    }

    return false;
}

/*
 * Loop all systems
 */
bool EcsLoop(EcsHeader * ecs)
{
    if(ecs)
    {
        for(ID i = 0; i < ecs->systems.length; ++i)
        {
            EcsSystemLoop(ecs, i, ENUM_EVENT_TYPE_DEFAULT);
        }
        return true;
    }
    return false;
}

bool EcsSystemLoop(EcsHeader * ecs, ID systemID, EventType eventType)
{
    System * system = VectorvAt(&ecs->systems, systemID);
    if(system)
    {
        if(!system->isActive || system->isDisposed)
        {
            return false;
        }
    }
    Vectorv componentPtrs;
    VectorvCreate(&componentPtrs, sizeof(Component *), NULL);

    VectorvDimension shortestComponentLength = (VectorvDimension) -1;
    Component * shortestComponent = NULL; // We loop trough shortest component, so there is no need for building separate looping list

    // Cache componentn pointers and get shortestComponent 
    for(ID i = 0; i < system->trackedComponentsIDs.length; ++i)
    {
        ID componentID = *((ID *) VectorvNthElement(&system->trackedComponentsIDs, i));
        Component * component = VectorvNthElement(&ecs->components, componentID);
        if(!component->isActive || component->isDisposed)
        {
            return false;
        }
        VectorvPush(&componentPtrs, &component);

        if(component->componentData.length < shortestComponentLength)
        {
            shortestComponent = component;
            shortestComponentLength = component->componentData.length;
        }
    }

    // If we got shortestComponent 
    if(shortestComponent)
    {
        Vectorv eventData;
        VectorvCreate(&eventData, sizeof(void *), NULL);

        register EntityIndex mapKeysLength = shortestComponent->mapKeys.length;

        // For every entity that is attached to shortestComponent 
        for(register EntityIndex i = 0; i < mapKeysLength; ++i)
        {
            //get entity index and ID
            EntityIndex entityIndex = *((EntityIndex *) VectorvNthElement(&shortestComponent->mapKeys, i));
            EntityID entityID = *((EntityID *) VectorvNthElement(&ecs->entities,entityIndex));

            if(EntityIsValid(entityID))
            {
                register ID componentPtrsLength = componentPtrs.length;
                
                // Loop trough all components that system is attached to and try to get component data of entityID
                for(register VectorvDimension j = 0; j < componentPtrsLength; ++j)  
                {
                    Component * component = *(Component **) VectorvNthElement(&componentPtrs, j);
                    void * componentData = ComponentGet(component, entityIndex); // Use internal function to skip error checking, as we checked for error before

                    if(componentData)
                    {
                        VectorvPush(&eventData, &componentData);
                    }else{
                        break; // If any of the components didn't contain entity data, we stop looping and exit. 
                    }
                }
                if(eventData.length == componentPtrsLength) // check if all component data was retrieved
                {
                    system->onEventFunc(eventType, entityID, &eventData);
                }
                eventData.length = 0;
            }
        }

        VectorvDestroy(&eventData);
        VectorvDestroy(&componentPtrs);
        return true;
    }
    return false;
}
