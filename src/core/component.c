#include "ecs.h"
#include "ecsinternal.h"

ID EcsComponentCreate(EcsHeader * ecs, size_t componentSize, void * defaultComponentData)
{
    if(ecs && defaultComponentData)
    {
        Component component;
        component.isActive= true;
        component.isDisposed = false;
        component.componentSize = componentSize;
        component.defaultComponentData = malloc(componentSize);

        if(component.defaultComponentData)
        {
            // this data is used when attaching entities, so that they have default data
            memcpy(component.defaultComponentData, defaultComponentData, component.componentSize);
            VectorvCreate(&component.componentData, componentSize, NULL); 
            VectorvCreate(&component.mapKeys, sizeof(EntityIndex), NULL);
            VectorvCreate(&component.mapSparse, sizeof(EntityIndex), NULL);
        
            VectorvPush(&ecs->components, &component);
            return ecs->components.length - 1;
        }
    }
    return ID_INVALID; 
}

/**
 * Adds entry to sparse map, update key map and copy defaultComponentData
 */
bool EcsEntityAttach(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    if(ecs && componentID != ID_INVALID)
    {
        if(ecs->components.length > componentID)
        {
            Component * component = VectorvNthElement(&ecs->components, componentID);
            EntityIndex entityIndex = EntityGetIndex(entityID);

            if(!component->isDisposed && entityIndex != ENTITY_INVALID_INDEX)
            {
                VectorvPush(&component->mapKeys, &entityIndex);
                VectorvPush(&component->componentData, component->defaultComponentData);

                // If attached entitie's id is higher than any that were attached before, we need to populate vector with invalid ids.
                while(component->mapSparse.length <= entityIndex) 
                {
                    EntityIndex entityIndexInvalid = ENTITY_INVALID_INDEX;
                    VectorvPush(&component->mapSparse, &entityIndexInvalid);
                }

                EntityIndex keyIndex = component->mapKeys.length - 1;  
                return VectorvSet(&component->mapSparse, entityIndex, &keyIndex);
            }
        }
    }
    return false;
}

/*
 * Sets id on sparse map to invalid and switches entitie's data and key with last one, so that vectors always remain densly packed.
 */
bool EcsEntityDetach(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    if(EcsEntityIsAttached(ecs, componentID, entityID))
    {
        EntityIndex entityIndex = EntityGetIndex(entityID);
        Component * component = VectorvNthElement(&ecs->components, componentID);

        if(component && !component->isDisposed)
        {
            EntityIndex * sparseKeyIndexRef = VectorvNthElement(&component->mapSparse, entityIndex);
            if(sparseKeyIndexRef)
            {
                //swap component data
                EntityIndex sparseKeyIndex = *sparseKeyIndexRef;
                void * lastComponentData = VectorvPeek(&component->componentData);
                if(lastComponentData != NULL)
                {
                    VectorvSet(&component->componentData, sparseKeyIndex, lastComponentData);
                    EntityIndex * indexKeysLast = VectorvPeek(&component->mapKeys);
                    //swap keys
                    VectorvSet(&component->mapSparse, *indexKeysLast, &sparseKeyIndex);
                    VectorvSet(&component->mapKeys, sparseKeyIndex, indexKeysLast);

                    VectorvPop(&component->componentData, NULL);
                    VectorvPop(&component->mapKeys, NULL);
                    *sparseKeyIndexRef = ENTITY_INVALID_INDEX;
                    
                    //if we removed the last entity in sparse map, let's shrink it
                    if(entityIndex == (component->mapSparse.length - 1))
                    {
                        EntityIndex sparseKeyIndex;
                        while(VectorvPop(&component->mapSparse, &sparseKeyIndex))
                        {
                            if(sparseKeyIndex != ENTITY_INVALID_INDEX)
                            {
                                // whoops, last one poped is valid, put it back.
                                VectorvPush(&component->mapSparse, &sparseKeyIndex);
                                break;
                            }
                        }
                    }
                    return true;
                }
            }
        }
        
    }
    return false;
}

/*
 * Just checks if component data is present and then updated provided reference to point to said data
 */
bool EcsComponentGet(EcsHeader *ecs, ID componentID, EntityID entityID, void **valueRef)
{
    if(EcsEntityIsAttached(ecs, componentID, entityID))
    {
        Component * component = VectorvNthElement(&ecs->components, componentID);
        void * componentData = ComponentGet(component, EntityGetIndex(entityID));
        if(componentData)
        {
            *valueRef = componentData;
            return true;
        }
    }
    return false;
}

/*
 * Internal equivalent of EcsComponentGet() skips checks for if an entity is attached to ecs
 */
void * ComponentGet(Component * component, EntityIndex entityIndex)
{
    if(!component->isDisposed && component->mapSparse.length > entityIndex)
    {
        EntityIndex sparseKeyIndex = *((EntityIndex *) VectorvNthElement(&component->mapSparse, entityIndex));

        if(sparseKeyIndex != ENTITY_INVALID_INDEX)
        {
            return VectorvNthElement(&component->componentData, sparseKeyIndex);
        }
    }
    return NULL;
}
/*
 * Checks if entity is attached, also if component is active (may be changed in the future).
 */
bool EcsEntityIsAttached(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    if(EcsEntityIsActive(ecs, entityID))
    {
        EntityIndex entityIndex = EntityGetIndex(entityID);
        Component * component = VectorvNthElement(&ecs->components, componentID);

        if(!component->isDisposed)
        {
            if(component->mapSparse.length > entityIndex)
            {
                return *((EntityIndex *) VectorvNthElement(&component->mapSparse, entityIndex)) != ENTITY_INVALID_INDEX;  
            }
        }
    }
    return false;
}

bool EcsComponentDestroy(EcsHeader * ecs, ID componentID)
{
    if(ecs && componentID != ID_INVALID)
    {
        Component * component = VectorvAt(&ecs->components, componentID);
        if(component)
        {
            ecs->components.disposeFunc(component);
            component->isDisposed = true;
            return true;
        }
    }
    return false;
}

bool EcsComponentActiveSet(EcsHeader * ecs, ID componentID, bool isActive)
{
    if(ecs && componentID != ID_INVALID)
    {
        Component * component = VectorvAt(&ecs->components, componentID);
        if(component)
        {
            component->isActive = isActive;
            return true;
        }
    }

    return false;
}
