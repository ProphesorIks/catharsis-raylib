#include "ecs.h"
#include "ecsinternal.h"

static inline void ComponentDestroy(void * componentRef);
static inline void SystemDestroy(void * systemRef);

bool EcsCreate(EcsHeader * ecs)
{
    if(ecs)
    {
        return 
        (
            VectorvCreate(&ecs->components, sizeof(Component), ComponentDestroy) &&
            VectorvCreate(&ecs->systems, sizeof(System), SystemDestroy) &&
            VectorvCreate(&ecs->entities, sizeof(EntityID), NULL) &&
            VectorvCreate(&ecs->entitiesRecycle, sizeof(EntityIndex), NULL)
        );
    }
    return false;
}

bool EcsDestroy(EcsHeader * ecs)
{
    if(ecs)
    {
        return  
        (
            VectorvDestroy(&ecs->components) &&
            VectorvDestroy(&ecs->systems) &&
            VectorvDestroy(&ecs->entities) &&
            VectorvDestroy(&ecs->entitiesRecycle)
        );
    }
    return false;
}

static inline void ComponentDestroy(void * componentRef)
{
    Component * component = componentRef; 
    if(!component->isDisposed)
    {
        VectorvDestroy(&component->componentData);
        VectorvDestroy(&component->mapKeys);
        VectorvDestroy(&component->mapSparse);

        free(component->defaultComponentData);

        component->isActive = false;
        component->componentSize = 0;
        component->isDisposed = true;
    }
}

static inline void SystemDestroy(void * systemRef)
{
    System * system = systemRef;
    if(!system->isDisposed)
    {
        VectorvDestroy(&system->trackedComponentsIDs);
        system->isActive = false;
        system->isDisposed = false;
    }
}

/*
 * New entity id is just incremented entity counter or recycled entity with version bump
 */
EntityID EcsEntityNext(EcsHeader * ecs)
{
    if(ecs)
    {
        if(ecs->entitiesRecycle.length > 0)
        {
            EntityIndex entityIndex;
            VectorvPop(&ecs->entitiesRecycle, &entityIndex);
            // sanity check. There shoud be no way we cache entity we didn't create
            assert(ecs->entities.length > entityIndex);
                
            EntityID * entityIDRef = VectorvNthElement(&ecs->entities, entityIndex);
            EntityID entityID = EntityCreateID(entityIndex, EntityGetVersion(*entityIDRef));

            VectorvSet(&ecs->entities, entityIndex, &entityID);
            return entityID;
            
        }
        else if(ecs->entities.length < ENTITIES_MAX)
        {
            EntityID entityID = EntityCreateID(ecs->entities.length, 0);
            VectorvPush(&ecs->entities, &entityID);
            return entityID;
        }
    }
    return EntityCreateID(ENTITY_INVALID_INDEX, 0);
}

/*
 * Loops all components and deactivates entity.
 */
bool EcsEntityDeactivate(EcsHeader * ecs, EntityID entityID)
{
    if(ecs && EntityIsValid(entityID)) 
    {
        EntityIndex entityIndex = EntityGetIndex(entityID);
        if(ecs->entities.length > entityIndex)
        {
            EntityID * entityIDRef = VectorvNthElement(&ecs->entities, entityIndex);
            if(entityIDRef)
            {
                if(*entityIDRef == entityID)
                {
                    for(ID i = 0; i < ecs->components.length; ++i)
                    {
                        EcsEntityDetach(ecs, i, entityID);
                    }
                    // Set entity to invalid and increment it's history
                    *entityIDRef = EntityCreateID(ENTITY_INVALID_INDEX, EntityGetVersion(*entityIDRef) + 1);
                    return VectorvPush(&ecs->entitiesRecycle, &entityIndex);
                }
            }
        }
    }
    return false;
}

bool EcsEntityIsActive(EcsHeader * ecs, EntityID entityID)
{
    if(ecs && EntityIsValid(entityID))
    {
        EntityIndex entityIndex = EntityGetIndex(entityID);
        if(ecs->entities.length > entityIndex)
        {
            return *((EntityID *) VectorvNthElement(&ecs->entities, entityIndex)) == entityID;
        }
    }
    return false;
}
