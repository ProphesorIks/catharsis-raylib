#include "ecs.h"
#include "ecsinternal.h"

static inline void VectorvExtend(Vectorv * vectorv);

bool VectorvCreate(Vectorv * vectorv, size_t elementSize, void (*onDispose)(void * element))
{
    if(vectorv)
    {
        vectorv->length = 0;
        vectorv->lengthAllocation = 0;
        vectorv->elementSize = elementSize;
        vectorv->elements = NULL;
        vectorv->disposeFunc = onDispose;
        return true;
    }

    return false;
}

bool VectorvDestroy(Vectorv * vectorv)
{
    if(vectorv)
    {
        if(vectorv->disposeFunc)
        {
            for(VectorvDimension i = 0; i < vectorv->length; ++i)
            {
                void * elem = VectorvNthElement(vectorv, i);
                vectorv->disposeFunc(elem);
            }
        }
        free(vectorv->elements);
        vectorv->elements = NULL;
        vectorv->length = 0;
        vectorv->elementSize = 0;
        return true;
    }
    return false;
}

bool VectorvPush(Vectorv * vectorv, void * element)
{
    if(vectorv->length < VECTORV_DIMENSION_MAX)
    {
        if(vectorv->length >= vectorv->lengthAllocation)
        {
            VectorvExtend(vectorv);
        }

        VectorvDimension index = vectorv->length++;
        memcpy(VectorvNthElement(vectorv, index), element, vectorv->elementSize);
        return true;
    }
    return false;
}

bool VectorvPop(Vectorv * vectorv, void * valueRef)
{
    if(vectorv)
    {
        if(vectorv->length > 0)
        {
            vectorv->length--;
            if(valueRef != NULL)
            {
                void * elementRef = VectorvNthElement(vectorv, vectorv->length);
                memcpy(valueRef, elementRef, vectorv->elementSize);
            }
            return true;
        } 
    }
    return false;
}

void * VectorvAt(Vectorv * vectorv, VectorvDimension index)
{
    if(vectorv)
    {
        if(index < vectorv->length) 
        {
            return VectorvNthElement(vectorv, index);
        }
    }
    return NULL; 
}

void * VectorvPeek(Vectorv * vectorv)
{
    if(vectorv)
    {
        if(vectorv->length > 0)
        {
            return VectorvNthElement(vectorv, vectorv->length-1);
        }
    }
    return NULL;
}


bool VectorvSet(Vectorv * vectorv, VectorvDimension index, void * ref)
{
    if(vectorv && ref)
    {
        while(index >= vectorv->lengthAllocation)
        {
            VectorvExtend(vectorv);
        }

        if(vectorv->length <= index)
        {
            vectorv->length = index+1;
        }
        memcpy(VectorvNthElement(vectorv, index), ref, vectorv->elementSize);
        return true;
    }
    return false;
}

static inline void VectorvExtend(Vectorv * vectorv)
{
    if(vectorv->lengthAllocation < UINT64_MAX)
    {
            vectorv->lengthAllocation *= 2;
            vectorv->lengthAllocation %= UINT64_MAX;
            vectorv->lengthAllocation = (vectorv->lengthAllocation > 0) ? vectorv->lengthAllocation : 1; 
            vectorv->elements = realloc(vectorv->elements, vectorv->elementSize * vectorv->lengthAllocation);
    }
}
