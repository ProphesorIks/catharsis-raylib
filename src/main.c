#include <stdio.h>
#include <string.h>
#include "raylib.h"
#include "core/ecs.h"

int main(int argc, char ** argv)
{
    if(argc >= 2)
    {
        if(strcmp(argv[1], "-test") == 0)
        {
            #include "tests/runner.h"
            return runTests();
        }
    }
    InitWindow(100, 100, "Catharsis");

    SetTargetFPS(60);

    while(!WindowShouldClose())
    {

        BeginDrawing();
        ClearBackground(GREEN);
        EndDrawing();
    }
    CloseWindow();
    return 0;
}
