#include "runner.h"

static int testsRun = 0;
static int assertionsRun = 0;

static int vectorTesting()
{
    // Create struct and shuffle some values
    struct testStruct{
        char a;
        float b;
    }elements[10] = {'a', 1.0};

    elements[4].a = 'b';
    elements[5].a = 'c';
    elements[6].b = 'd';

    // Test creation
    Vectorv vector;
    _assert(!VectorvCreate(NULL, 10, NULL));
    _assert(VectorvCreate(&vector, sizeof(struct testStruct), NULL));
    _assert(vector.length == 0);
    _assert(vector.lengthAllocation == 0);
    _assert(vector.elementSize == sizeof(struct testStruct));
    _assert(!VectorvPop(&vector, NULL));
    _assert(VectorvAt(&vector, 4) == NULL);

    // Test pushed data
    for(int i = 0; i < 7; i++)
    {
        _assert(VectorvPush(&vector, elements+i));   
    }
    _assert(vector.length == 7);
    _assert(((struct testStruct *) VectorvAt(&vector, 5))->a == elements[5].a);
    _assert(((struct testStruct *) VectorvAt(&vector, 4))->a == elements[4].a);
    _assert(((struct testStruct *) VectorvAt(&vector, 0))->a == elements[0].a);
    _assert(((struct testStruct *) VectorvAt(&vector, 1334)) == NULL);
    _assert(((struct testStruct *) VectorvPeek(&vector))->b == 'd');

    // Test popping
    struct testStruct ref;

    _assert(VectorvPop(&vector, NULL));
    _assert(VectorvPop(&vector, &ref));
    _assert(ref.a == elements[5].a);
    _assert(vector.length == 5);

    for(int i = 0; i < 30; i++)
    {
        if(i > 4)
        {
            _assert(!VectorvPop(&vector, NULL));
        }
        else
        {
            _assert(VectorvPop(&vector, NULL));
        }
    }
    _assert(vector.length == 0);
    _assert(VectorvSet(&vector, 1000, elements));
    _assert(((struct testStruct *) VectorvAt(&vector, 1000))->a == elements[0].a);
    _assert(vector.length > 1000);

    _assert(VectorvDestroy(&vector));
    _assert(!VectorvDestroy(NULL));
    return 0;
}

static int ecsTesting()
{
    EcsHeader ecs;
    _assert(!EcsCreate(NULL));
    _assert(EcsCreate(&ecs));
    _assert(ecs.components.length == 0);
    _assert(ecs.entities.length == 0);
    _assert(ecs.entitiesRecycle.length == 0);

    _assert(!EcsDestroy(NULL));

    _assert(EcsEntityNext(&ecs) == EntityCreateID(0,0));
    _assert(EcsEntityNext(&ecs) == EntityCreateID(1,0));
    _assert(ecs.entities.length == 2);

    _assert(EcsEntityDeactivate(&ecs, EntityCreateID(0,0)));
    _assert(!EcsEntityDeactivate(&ecs, EntityCreateID(0,0)));
    _assert(ecs.entities.length == 2);
    _assert(ecs.entitiesRecycle.length == 1);

    _assert(EcsEntityNext(&ecs) == EntityCreateID(0,1));
    _assert(ecs.entities.length == 2);
    _assert(ecs.entitiesRecycle.length == 0);

    _assert(EcsEntityDeactivate(&ecs, EntityCreateID(0,1)));
    _assert(EcsEntityNext(&ecs) == EntityCreateID(0,2));
    _assert(EcsEntityNext(&ecs) == EntityCreateID(2,0));
    _assert(!EcsEntityDeactivate(&ecs, EntityCreateID(ENTITY_INVALID_INDEX, 0)));
    
    for(int i = 0; i < 10000; i++)
    {
        EcsEntityNext(&ecs);
    }
    _assert(ecs.entities.length == 10003);

    for(int i = 0; i < 20; i++)
    {
        EcsEntityDeactivate(&ecs, EntityCreateID(i+10, 0));
    }
    _assert(EcsEntityNext(&ecs) == EntityCreateID(29,1));

    for(int i = 0; i < 20000; i++)
    {
        EcsEntityDeactivate(&ecs, EntityCreateID(i, 0));
    }

    _assert(EcsDestroy(&ecs));
    return 0;
}

static int componentAttachDetachTesting()
{
    EcsHeader ecs;
    EcsCreate(&ecs);
    
    // default data
    CmpPosition pos = {1,2,3};
    CmpColor col = {255,100,50,0};
    
    // check if components are created correctly
    _assert(EcsComponentCreate(&ecs, sizeof(CmpPosition), &pos) == 0);
    _assert(EcsComponentCreate(NULL, sizeof(CmpPosition), &pos) == ID_INVALID);
    _assert(EcsComponentCreate(NULL, sizeof(CmpPosition), NULL) == ID_INVALID);
    _assert(EcsComponentCreate(&ecs, sizeof(CmpColor), &col) == 1);
    _assert(ecs.components.length == 2);
    Component * cpmPos = VectorvAt(&ecs.components, 0);
    Component * cpmCol = VectorvAt(&ecs.components, 1);
    _assert(cpmPos->componentSize == sizeof(CmpPosition));
    _assert(cpmCol->componentSize == sizeof(CmpColor));

    // check if entity attachment validates arguments
    _assert(EcsEntityAttach(&ecs, 0, EntityCreateID(0,0)));
    EntityID e0 = EcsEntityNext(&ecs);
    _assert(EcsEntityAttach(&ecs, 0, EcsEntityNext(&ecs)));
    _assert(EcsEntityAttach(&ecs, 1, EcsEntityNext(&ecs)));
    _assert(!EcsEntityAttach(&ecs, 2, EcsEntityNext(&ecs)));
    _assert(EcsEntityAttach(&ecs, 0, EcsEntityNext(&ecs)));

    //check if keys were updated and if correct data is present
    _assert(cpmPos->mapKeys.length == 3);
    _assert(((CmpPosition *) (cpmPos->componentData.elements))->y == pos.y);
    _assert(((CmpColor *) (cpmCol->componentData.elements))->g == col.g);

    // check if sparse map is sized correctly
    EntityIndex * index = ((Component *) ecs.components.elements)->mapSparse.elements;
    _assert(*(index + 3) == ENTITY_INVALID_INDEX);
    _assert(cpmPos->mapSparse.length == 5);

    _assert(EcsEntityIsAttached(&ecs, 0, e0));
    _assert(!EcsEntityIsAttached(&ecs, 0, EntityCreateID(0,2020)));
    _assert(EcsEntityDetach(&ecs, 0, e0));
    _assert(!EcsEntityIsAttached(&ecs, 0, e0));

    // check if maps are sized correctly
    _assert(cpmPos->mapSparse.length == 5);
    _assert(cpmPos->mapKeys.length == 2);
    _assert(ecs.components.length == 2);

    // check if after detach map shrinks
    _assert(EcsEntityDetach(&ecs, 0, EntityCreateID(4, 0)));
    _assert(cpmPos->mapSparse.length == 2);
    EntityID e1 = EcsEntityNext(&ecs);
    _assert(cpmPos->mapKeys.length == 1);
    _assert(cpmCol->mapKeys.length == 1);
    _assert(EcsEntityAttach(&ecs, 0, e1));
    _assert(EcsEntityAttach(&ecs, 1, e1));
    _assert(cpmPos->mapKeys.length == 2);
    _assert(cpmCol->mapKeys.length == 2);

    // check if component data is fetched correctly
    void * cmpRef;
    _assert(EcsComponentGet(&ecs, 0, e1, &cmpRef));
    _assert(cmpRef != NULL);
    _assert(EcsComponentGet(&ecs, 1, e1, &cmpRef));
    _assert(cmpRef != NULL);
    
    // check entity detacment and map sizes once again
    _assert(EcsEntityDeactivate(&ecs, e1));
    _assert(cpmPos->mapKeys.length == 1);
    _assert(cpmCol->mapKeys.length == 1);

    // check if non existing components warn about non existing
    _assert(!EcsComponentGet(&ecs, 0, e1, &cmpRef));
    _assert(!EcsComponentGet(&ecs, 1, e1, &cmpRef));

    // test component activation
    _assert(EcsComponentActiveSet(&ecs, 0, false));
    _assert(EcsComponentActiveSet(&ecs, 0, false));
    _assert(!EcsComponentActiveSet(&ecs, 2002, false));
    _assert(EcsComponentActiveSet(&ecs, 1, false));
    _assert(EcsDestroy(&ecs));
    return 0;
}

static int componetDataTesting()
{
    EcsHeader ecs;
    CmpPosition cmpPositionDefault = {12.0,1337.3,10.0};
    CmpColor cmpColorDefault = {255,200,34,0};
    _assert(EcsCreate(&ecs));
    ID posId = ID_INVALID;
    ID colId = ID_INVALID;

    // create components
    _assert((posId = EcsComponentCreate(&ecs, sizeof(CmpPosition), &cmpPositionDefault)) == 0);
    _assert((colId = EcsComponentCreate(&ecs, sizeof(CmpColor), &cmpColorDefault)) == 1);

    // attach entities
    for(int i = 0; i < 20; i++)
    {
        EcsEntityNext(&ecs);
    }
    _assert(ecs.entities.length == 20);

    // detach some entities to make mappings more complex
    for(int i = 0; i < 10; i++)
    {
        _assert(EcsEntityDeactivate(&ecs, EntityCreateID(i + 5,0)));
    }
    _assert(ecs.entities.length == 20);
    _assert(EcsEntityNext(&ecs) == EntityCreateID(14,1));


    // create test entities and check their data
    EntityID e0 = EcsEntityNext(&ecs);
    EntityID e1 = EcsEntityNext(&ecs);
    EntityID e2 = EcsEntityNext(&ecs);
    EntityID e3 = EcsEntityNext(&ecs);
    
    _assert(posId == 0 && colId == 1);
    _assert(EcsEntityAttach(&ecs, posId, e0));
    _assert(EcsEntityAttach(&ecs, posId, e1));
    _assert(EcsEntityAttach(&ecs, posId, e2));

    _assert(EcsEntityAttach(&ecs, colId, e2));
    _assert(EcsEntityAttach(&ecs, colId, e3));

    Component * cmpPosRef = VectorvAt(&ecs.components, posId);
    Component * cmpColRef = VectorvAt(&ecs.components, colId);

    _assert(cmpPosRef->mapKeys.length == 3);
    _assert(cmpPosRef->mapSparse.length == 14);

    _assert(cmpColRef->mapKeys.length == 2);
    _assert(cmpColRef->mapSparse.length == 12);

    _assert(EcsEntityDetach(&ecs,posId, e0));
    _assert(cmpPosRef->mapKeys.length == 2);
    CmpPosition * pos = NULL; 
    _assert(!EcsComponentGet(&ecs, posId, e0, (void **)&pos));
    _assert(EcsComponentGet(&ecs, posId, e1, (void **) &pos));
    _assert(pos != NULL);
    _assert(pos->x == cmpPositionDefault.x);

    CmpColor * colE2 = NULL;
    CmpColor * colE3 = NULL;

    _assert(EcsComponentGet(&ecs, colId, e2, (void **)&colE2));
    _assert(EcsComponentGet(&ecs, colId, e3, (void **)&colE3));

    // check if default data is present
    _assert(colE2->b == cmpColorDefault.b);
    _assert(colE3->r == cmpColorDefault.r);
    
    // alter some data
    colE2->r = 15;
    colE3->g = 1410;
    EntityID e4 = EcsEntityNext(&ecs);

    // shake up component mapping once again
    _assert(EcsEntityAttach(&ecs, colId, e4));
    _assert(EcsEntityDetach(&ecs, colId, e2));
    _assert(cmpColRef->mapKeys.length == 2);
    _assert(EcsEntityDetach(&ecs, colId, e4));
    _assert(EcsComponentGet(&ecs, colId, e3, (void **)&colE3));
    
    // after detaching and attaching, component data references should be correct
    _assert(colE3 != NULL);
    _assert(colE3->r != 15);
    _assert(colE3->g == 1410);
    
    _assert(EcsEntityDetach(&ecs, colId, e3));
    _assert(!EcsEntityDetach(&ecs, colId, e3));
    _assert(ecs.components.length == 2);
    _assert(EcsDestroy(&ecs));
    _assert(ecs.components.length == 0);
    return 0;
}

// function to test system looping
static int systemLoopCount = 0;
static void * onEventFuncSys0(EventType event, EntityID entity, Vectorv * eventData)
{
    if(event == ENUM_EVENT_TYPE_DEFAULT)
    {
        //cmp pos and color are available
        if(eventData->length == 2) 
        {
            CmpPosition * pos = *((CmpPosition **) VectorvAt(eventData, 0));
            CmpColor * col = *((CmpColor **) VectorvAt(eventData, 1));
        }
        else
        {
            CmpPosition * pos = *((CmpPosition **) VectorvAt(eventData, 0));
        }
        systemLoopCount++;
    }
    return NULL;
}

static int systemTesting()
{
    EcsHeader ecs; 
    _assert(EcsCreate(&ecs));

    // default components
    CmpPosition posDefault = {11.0, 17.2, 321.94};
    CmpColor colDefault = {19, 172, 14, 17};

    ID cmpPosID = EcsComponentCreate(&ecs, sizeof(CmpPosition), &posDefault);
    ID cmpColID = EcsComponentCreate(&ecs, sizeof(CmpColor), &colDefault);
    _assert(cmpPosID == 0 && cmpColID == 1);

    // create headers
    Vectorv sys0Headers;
    VectorvCreate(&sys0Headers, sizeof(ID), NULL);
    VectorvPush(&sys0Headers, &cmpPosID);
    VectorvPush(&sys0Headers, &cmpColID);

    Vectorv sys1Headers;
    VectorvCreate(&sys1Headers, sizeof(ID), NULL);
    VectorvPush(&sys1Headers, &cmpPosID);

    //generate complex entity mapping
    for(int i = 0; i < 20; i++)
    {
        EntityID id = EcsEntityNext(&ecs);
        EcsEntityAttach(&ecs, cmpPosID, id);
        if(i%3)
        {
            EcsEntityAttach(&ecs, cmpColID, id);
        }

        if(i/5 == 1)
        {
            EcsEntityDetach(&ecs, cmpPosID, id);
            EcsEntityDetach(&ecs, cmpColID, id);
        }
    }

    // set some entities data
    CmpPosition * pospos;
    EcsComponentGet(&ecs, cmpPosID, EntityCreateID(2,0),(void **) &pospos);
    pospos->y = 55.55;

    // create first system
    ID sys1ID;
    _assert(EcsSystemCreate(&ecs, onEventFuncSys0, "aa", 2, cmpPosID, cmpPosID) != ID_INVALID);
    _assert((sys1ID = EcsSystemCreate(&ecs, onEventFuncSys0, "a", 1, cmpPosID)) != ID_INVALID);
    System * sys1 = VectorvAt(&ecs.systems, sys1ID);
    _assert(sys1->trackedComponentsIDs.length == sys1Headers.length);
    
    // loop while detaching 
    for(int i = 0; i < 10; i++)
    {
        if(i == 6)
        {
            EcsEntityDetach(&ecs, cmpPosID, EntityCreateID(0,0));
            EcsEntityDetach(&ecs, cmpColID, EntityCreateID(0,0));
        }
        EcsLoop(&ecs);
    }

    _assert(systemLoopCount == 292);
   
    // loop once again with detaching
    for(int i = 0; i < 10; i++)
    {
        EntityID id = *((EntityID *) VectorvAt(&ecs.entities, ecs.entities.length -1));
        EcsEntityDetach(&ecs, cmpPosID, id);
        EcsLoop(&ecs);
    }

    _assert(systemLoopCount == 552);

    //previous loop count
    int sysLoopCountPrev = systemLoopCount;
    _assert(EcsComponentActiveSet(&ecs, cmpPosID, false));
    for(int i = 0; i < 10; i++)
    {
        EcsLoop(&ecs);
    }

    _assert(sysLoopCountPrev == systemLoopCount);
    _assert(EcsComponentActiveSet(&ecs, cmpPosID, true));
    EcsLoop(&ecs);
    _assert(systemLoopCount == sysLoopCountPrev + 26);

    _assert(VectorvDestroy(&sys0Headers));
    _assert(VectorvDestroy(&sys1Headers));
    _assert(EcsDestroy(&ecs));
    return 0;
}

// empty loop functions
static void * loopSystemOne(EventType event, EntityID entity, Vectorv * eventData)
{ 
    CmpPosition * pos = *(CmpPosition **) VectorvAt(eventData, 0);
    assert(pos->x == 0.0f);
    assert(pos->y == 12.19f);
    assert(pos->z == 290.29f);
    return NULL;
}
static void * loopSystemTwo(EventType event, EntityID entity, Vectorv * eventData) 
{
    CmpPosition * pos = *(CmpPosition **) VectorvAt(eventData, 0);
    CmpColor * col = *(CmpColor **) VectorvAt(eventData, 1);
    assert(pos->x == 0.0f);
    assert(pos->y == 12.19f);
    assert(pos->z == 290.29f);
    assert(col->r == 255);
    assert(col->g == 155);
    assert(col->b == 55);
    assert(col->a == 251);

    return NULL;
}

static int performanceTesting()
{
    EcsHeader ecs;
    EcsCreate(&ecs);
    CmpPosition cmpDefaultPosition = {0.0f,12.19f, 290.29f};
    CmpColor cmpDefaultColor = {255,155,55,251};
    
    // create components
    ID cmpIdPosition = EcsComponentCreate(&ecs, sizeof(CmpPosition), &cmpDefaultPosition);
    ID cmpIdColor = EcsComponentCreate(&ecs, sizeof(CmpColor), &cmpDefaultColor);
    
    // create systems and attach components to them
    Vectorv sysTracks;
    VectorvCreate(&sysTracks, sizeof(ID), NULL);
    VectorvPush(&sysTracks, &cmpIdPosition);

    ID sysMove = EcsSystemCreate(&ecs, loopSystemOne, "a", 1, cmpIdPosition);

    VectorvPush(&sysTracks, &cmpIdColor);
    ID sysDraw = EcsSystemCreate(&ecs, loopSystemTwo, "aa", 2, cmpIdPosition, cmpIdColor);
    
    VectorvDestroy(&sysTracks);

    // create 10K entities
    for(EntityIndex i = 0; i < 10000; i++)
    {
        EntityID entityID = EcsEntityNext(&ecs);
        EcsEntityAttach(&ecs, cmpIdPosition, entityID);
        EcsEntityAttach(&ecs, cmpIdColor, entityID);
    }
    printf("Created: %lu entities\n", ecs.entities.length);

    // loop trough entities and record time
    int ecsLoopCount = 1000;
    time_t t;
    t = clock();
    for(int i = 0; i < ecsLoopCount; i++)
    {
        EcsLoop(&ecs);
    }
    t = (clock() - t);
    double timeTaken = (t/(double) ecsLoopCount)/CLOCKS_PER_SEC;
    //print time results
    printf("For %lu systems with %lu entities looping %d times took %gs\nAverage EcsLoop time was: %gs\n", ecs.systems.length, ecs.entities.length, ecsLoopCount, (double)t/CLOCKS_PER_SEC, timeTaken);

    // assert that one loop took less 50ms (Very bad case, should never exceed that) 5ms is preferrable;
    _assert(timeTaken < 0.05);
    _assert(EcsDestroy(&ecs));
    return 0;
}

static int testTesting()
{
    _assert(1 == 1);
    _assert(1 != 0);
    return 0;
}

static int allTests(void)
{
    _verify(testTesting);
    _verify(vectorTesting);
    _verify(ecsTesting);
    _verify(componentAttachDetachTesting);
    _verify(componetDataTesting);
    _verify(systemTesting);
    _verify(performanceTesting);
    return 0;
}

int runTests(void)
{
    setbuf(stdout, NULL);
    printf("\nSTARTING TESTING...\n");
    int testsPassed = allTests();
    printf("\n\e[33mRESULTS:\e[0m\n");
    printf("\tAssertions run:\t%8d\n", assertionsRun);
    printf("\tTests run:\t%8d\n", testsRun);

    if (testsPassed == 0)
    {
       printf("\t\e[32mALL PASSED\e[0m\n"); 
    }
    else
    {
        printf("\t\e[31mSOME TESTS FAILED\e[0m\n");
    }
    return testsPassed != 0;
}
