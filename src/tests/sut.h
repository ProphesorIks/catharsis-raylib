#ifndef SUT_H
#define SUT_H

#define FAIL() printf("\n\e[41mfailure in %s() line %d\e[39m\e[49m\n", __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; }else{assertionsRun++;} } while(0)
#define _verify(test) do { int r=test(); testsRun++; if(r) return r; } while(0)

#endif