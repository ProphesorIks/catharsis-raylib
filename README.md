### Catharsis raylib
This is remaster of a gmtk2020 gamejam entry catharsis. While original was
written in JS, this one is written in c with raylib and my custom ECS.
The purpose of this project is to test out how my ECS holds out in production
and find and fix ecs bugs. It should also be a fine way for me to test out
building for different platforms (Web, Windows and Linux)

[Original gamejam entry](https://prophersroiks.itch.io/catharsis)


[ECS](https://gitlab.com/ProphesorIks/raylib-entity-component-system)
