#!/bin/bash
set -e

case $1 in
    "release")
        pushd build;
        cmake -DCMAKE_BUILD_TYPE=Release ..;
        make;
        popd;
        ./build/catharsis
        ;;
    "test")
        pushd build;
        cmake -DCMAKE_BUILD_TYPE=Debug ..;
        make;
        popd;
        ./build/catharsis -test;
        ;;
    "clean")
        rm -rf build/Makefile build/ecs;
        rm -rf vgcore.*
        ;;
    *)
        ./build/catharsis
        ;;
esac
