
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/calisto/Projects/c/catharsis-raylib/src/core/component.c" "CMakeFiles/catharsis.dir/src/core/component.c.o" "gcc" "CMakeFiles/catharsis.dir/src/core/component.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/core/ecs.c" "CMakeFiles/catharsis.dir/src/core/ecs.c.o" "gcc" "CMakeFiles/catharsis.dir/src/core/ecs.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/core/entity.c" "CMakeFiles/catharsis.dir/src/core/entity.c.o" "gcc" "CMakeFiles/catharsis.dir/src/core/entity.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/core/system.c" "CMakeFiles/catharsis.dir/src/core/system.c.o" "gcc" "CMakeFiles/catharsis.dir/src/core/system.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/core/vectorv.c" "CMakeFiles/catharsis.dir/src/core/vectorv.c.o" "gcc" "CMakeFiles/catharsis.dir/src/core/vectorv.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/main.c" "CMakeFiles/catharsis.dir/src/main.c.o" "gcc" "CMakeFiles/catharsis.dir/src/main.c.o.d"
  "/home/calisto/Projects/c/catharsis-raylib/src/tests/runner.c" "CMakeFiles/catharsis.dir/src/tests/runner.c.o" "gcc" "CMakeFiles/catharsis.dir/src/tests/runner.c.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
