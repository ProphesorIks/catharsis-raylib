# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.20

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/calisto/Projects/c/catharsis-raylib

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/calisto/Projects/c/catharsis-raylib/build

# Include any dependencies generated for this target.
include CMakeFiles/catharsis.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/catharsis.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/catharsis.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/catharsis.dir/flags.make

CMakeFiles/catharsis.dir/src/core/component.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/core/component.c.o: ../src/core/component.c
CMakeFiles/catharsis.dir/src/core/component.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object CMakeFiles/catharsis.dir/src/core/component.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/core/component.c.o -MF CMakeFiles/catharsis.dir/src/core/component.c.o.d -o CMakeFiles/catharsis.dir/src/core/component.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/core/component.c

CMakeFiles/catharsis.dir/src/core/component.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/core/component.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/core/component.c > CMakeFiles/catharsis.dir/src/core/component.c.i

CMakeFiles/catharsis.dir/src/core/component.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/core/component.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/core/component.c -o CMakeFiles/catharsis.dir/src/core/component.c.s

CMakeFiles/catharsis.dir/src/core/ecs.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/core/ecs.c.o: ../src/core/ecs.c
CMakeFiles/catharsis.dir/src/core/ecs.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building C object CMakeFiles/catharsis.dir/src/core/ecs.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/core/ecs.c.o -MF CMakeFiles/catharsis.dir/src/core/ecs.c.o.d -o CMakeFiles/catharsis.dir/src/core/ecs.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/core/ecs.c

CMakeFiles/catharsis.dir/src/core/ecs.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/core/ecs.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/core/ecs.c > CMakeFiles/catharsis.dir/src/core/ecs.c.i

CMakeFiles/catharsis.dir/src/core/ecs.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/core/ecs.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/core/ecs.c -o CMakeFiles/catharsis.dir/src/core/ecs.c.s

CMakeFiles/catharsis.dir/src/core/entity.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/core/entity.c.o: ../src/core/entity.c
CMakeFiles/catharsis.dir/src/core/entity.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building C object CMakeFiles/catharsis.dir/src/core/entity.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/core/entity.c.o -MF CMakeFiles/catharsis.dir/src/core/entity.c.o.d -o CMakeFiles/catharsis.dir/src/core/entity.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/core/entity.c

CMakeFiles/catharsis.dir/src/core/entity.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/core/entity.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/core/entity.c > CMakeFiles/catharsis.dir/src/core/entity.c.i

CMakeFiles/catharsis.dir/src/core/entity.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/core/entity.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/core/entity.c -o CMakeFiles/catharsis.dir/src/core/entity.c.s

CMakeFiles/catharsis.dir/src/core/system.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/core/system.c.o: ../src/core/system.c
CMakeFiles/catharsis.dir/src/core/system.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building C object CMakeFiles/catharsis.dir/src/core/system.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/core/system.c.o -MF CMakeFiles/catharsis.dir/src/core/system.c.o.d -o CMakeFiles/catharsis.dir/src/core/system.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/core/system.c

CMakeFiles/catharsis.dir/src/core/system.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/core/system.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/core/system.c > CMakeFiles/catharsis.dir/src/core/system.c.i

CMakeFiles/catharsis.dir/src/core/system.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/core/system.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/core/system.c -o CMakeFiles/catharsis.dir/src/core/system.c.s

CMakeFiles/catharsis.dir/src/core/vectorv.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/core/vectorv.c.o: ../src/core/vectorv.c
CMakeFiles/catharsis.dir/src/core/vectorv.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building C object CMakeFiles/catharsis.dir/src/core/vectorv.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/core/vectorv.c.o -MF CMakeFiles/catharsis.dir/src/core/vectorv.c.o.d -o CMakeFiles/catharsis.dir/src/core/vectorv.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/core/vectorv.c

CMakeFiles/catharsis.dir/src/core/vectorv.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/core/vectorv.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/core/vectorv.c > CMakeFiles/catharsis.dir/src/core/vectorv.c.i

CMakeFiles/catharsis.dir/src/core/vectorv.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/core/vectorv.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/core/vectorv.c -o CMakeFiles/catharsis.dir/src/core/vectorv.c.s

CMakeFiles/catharsis.dir/src/main.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/main.c.o: ../src/main.c
CMakeFiles/catharsis.dir/src/main.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Building C object CMakeFiles/catharsis.dir/src/main.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/main.c.o -MF CMakeFiles/catharsis.dir/src/main.c.o.d -o CMakeFiles/catharsis.dir/src/main.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/main.c

CMakeFiles/catharsis.dir/src/main.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/main.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/main.c > CMakeFiles/catharsis.dir/src/main.c.i

CMakeFiles/catharsis.dir/src/main.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/main.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/main.c -o CMakeFiles/catharsis.dir/src/main.c.s

CMakeFiles/catharsis.dir/src/tests/runner.c.o: CMakeFiles/catharsis.dir/flags.make
CMakeFiles/catharsis.dir/src/tests/runner.c.o: ../src/tests/runner.c
CMakeFiles/catharsis.dir/src/tests/runner.c.o: CMakeFiles/catharsis.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Building C object CMakeFiles/catharsis.dir/src/tests/runner.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/catharsis.dir/src/tests/runner.c.o -MF CMakeFiles/catharsis.dir/src/tests/runner.c.o.d -o CMakeFiles/catharsis.dir/src/tests/runner.c.o -c /home/calisto/Projects/c/catharsis-raylib/src/tests/runner.c

CMakeFiles/catharsis.dir/src/tests/runner.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/catharsis.dir/src/tests/runner.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/calisto/Projects/c/catharsis-raylib/src/tests/runner.c > CMakeFiles/catharsis.dir/src/tests/runner.c.i

CMakeFiles/catharsis.dir/src/tests/runner.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/catharsis.dir/src/tests/runner.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/calisto/Projects/c/catharsis-raylib/src/tests/runner.c -o CMakeFiles/catharsis.dir/src/tests/runner.c.s

# Object files for target catharsis
catharsis_OBJECTS = \
"CMakeFiles/catharsis.dir/src/core/component.c.o" \
"CMakeFiles/catharsis.dir/src/core/ecs.c.o" \
"CMakeFiles/catharsis.dir/src/core/entity.c.o" \
"CMakeFiles/catharsis.dir/src/core/system.c.o" \
"CMakeFiles/catharsis.dir/src/core/vectorv.c.o" \
"CMakeFiles/catharsis.dir/src/main.c.o" \
"CMakeFiles/catharsis.dir/src/tests/runner.c.o"

# External object files for target catharsis
catharsis_EXTERNAL_OBJECTS =

catharsis: CMakeFiles/catharsis.dir/src/core/component.c.o
catharsis: CMakeFiles/catharsis.dir/src/core/ecs.c.o
catharsis: CMakeFiles/catharsis.dir/src/core/entity.c.o
catharsis: CMakeFiles/catharsis.dir/src/core/system.c.o
catharsis: CMakeFiles/catharsis.dir/src/core/vectorv.c.o
catharsis: CMakeFiles/catharsis.dir/src/main.c.o
catharsis: CMakeFiles/catharsis.dir/src/tests/runner.c.o
catharsis: CMakeFiles/catharsis.dir/build.make
catharsis: /usr/local/lib/libraylib.so
catharsis: CMakeFiles/catharsis.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Linking C executable catharsis"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/catharsis.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/catharsis.dir/build: catharsis
.PHONY : CMakeFiles/catharsis.dir/build

CMakeFiles/catharsis.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/catharsis.dir/cmake_clean.cmake
.PHONY : CMakeFiles/catharsis.dir/clean

CMakeFiles/catharsis.dir/depend:
	cd /home/calisto/Projects/c/catharsis-raylib/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/calisto/Projects/c/catharsis-raylib /home/calisto/Projects/c/catharsis-raylib /home/calisto/Projects/c/catharsis-raylib/build /home/calisto/Projects/c/catharsis-raylib/build /home/calisto/Projects/c/catharsis-raylib/build/CMakeFiles/catharsis.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/catharsis.dir/depend

